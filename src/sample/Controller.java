package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;

import java.awt.*;


public class Controller<privat> {

    @FXML
    TextField firstNumber;

    @FXML
    TextField secondNumber;

    @FXML
    TextField operator;

    @FXML
    Label result;

    @FXML
    private void count() {
        double numberOne = Double.parseDouble(firstNumber.getText());
        double numberTwo = Double.parseDouble(secondNumber.getText());
        String symbol = operator.getText();


        result.setText(Account.account(numberOne,numberTwo,symbol));
    }


}
