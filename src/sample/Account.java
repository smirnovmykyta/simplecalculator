package sample;

public class Account {

    public static String account(double numberOne, double numberTwo, String symbol) {
        double result;

        switch (symbol) {
            case "+":
                result = numberOne + numberTwo;
                return String.valueOf(result);
            case "-":
                result = numberOne - numberTwo;
                return String.valueOf(result);
            case "*":
                result = numberOne * numberTwo;
                return String.valueOf(result);
            case "/":
                if (numberTwo == 0) {
                    return "На ноль делить не умею.";
                }
                result = numberOne / numberTwo;
                return String.valueOf(result);
            default:
                result = 0;
                return String.valueOf(result);
        }
    }
}
